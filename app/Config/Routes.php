<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

$routes->get('permohonan/pelajar', 'Permohonan\Pelajar::index');

$routes->get('permohonan/pelajar/(:num)','Permohonan\Pelajar::show/$1');

$routes->group('admin-permohonan', ['filter'=>'authGuard:002,001','namespace' => 'App\Controllers\Permohonan'], function ($routes) {
    $routes->resource('pelajar');
});

$routes->get('register', 'User::index');

$routes->post('register/create', 'User::create');

$routes->get('register/show/(:num)', 'User::show/$1');

$routes->get('login', 'User::login');

$routes->post('loginin', 'User::loginin');

$routes->get('logout', 'User::logout',['filter'=>'authGuard']);

// $routes->post('course', 'Course::create');
// $routes->get('course', 'Course::new');

// $routes->add('test', 'Course::new');
// $routes->resource('users');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
