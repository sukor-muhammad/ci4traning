<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Student extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                    'type'           => 'INT',
                    'auto_increment' => true,
            ],
            'name'       => [
                'type' => 'VARCHAR',
                'constraint' => 255
                    
            ],
            'address' => [
                    'type' => 'TEXT',
                  
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 255
              
            ],
            'ic' => [
                'type' => 'VARCHAR',
                'constraint' => 12
              
            ],
            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'update_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'delete_at DATETIME DEFAULT NULL',
        ]);

            $this->forge->addKey('id', true);
                $this->forge->createTable('student');
      

    }

    public function down()
    {
        $this->forge->dropTable('student');
    }
}
