<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class StudentApps extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                    'type'           => 'INT',
                    'unsigned'       => true,
                    'auto_increment' => true,
            ],
            'id_student'       => [
                'type' => 'INT',
         
                    
            ],
            'id_course1' => [
                'type' => 'INT',
                  
            ],
            'id_course2' => [
                'type' => 'INT',
              
            ],
            'id_course3' => [
                'type' => 'INT',
              
            ],



            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'update_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'delete_at DATETIME DEFAULT NULL',
        ]);

                $this->forge->addKey('id', true);
                $this->forge->addForeignKey('id_student','student','id');
                $this->forge->addForeignKey('id_course1','course','id');
                $this->forge->addForeignKey('id_course2','course','id');
                $this->forge->addForeignKey('id_course3','course','id');
                $this->forge->createTable('student_apps');

            
      

    }

    public function down()
    {
        $this->forge->dropTable('student_apps');
    }
}
