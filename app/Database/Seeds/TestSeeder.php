<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class TestSeeder extends Seeder
{
        public function run()
        {
                 $this->call('Course');
                 $this->call('StudentSeeder');
                 $this->call('StudentAppsSeeder');
                
        
        }
}