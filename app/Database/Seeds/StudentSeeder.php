<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;


class StudentSeeder extends Seeder
{
    public function run()
    {
        //

       for ($i=0; $i <10000 ; $i++) { 
            # code...
     
        $data = [

            'name' => static::faker('ms_MY')->name('ms_MY'),
            'address'    => static::faker('ms_MY')->address,
            'email'=>static::faker('ms_MY')->email,
            'ic'=>str_replace('-','',static::faker('ms_MY')->ssn)

];



    // Simple Queries
   
        // $this->db->query("INSERT INTO student (name,address,email,ic) VALUES(:name:,:address,:email,:ic)", $data);
    
        $this->db->table('student')->insert($data);
    
   }


    }
}
