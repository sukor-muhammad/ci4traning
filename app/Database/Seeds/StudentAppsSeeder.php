<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class StudentAppsSeeder extends Seeder
{
    public function run()
    {

       $data= $this->db->table('student')->get();

   

       foreach($data->getResult() as $row){
       
                $data=[
                    
                    'id_student'=>$row->id,
                    'id_course1'=>rand(1,2),
                    'id_course2'=>rand(3,4),
                    'id_course3'=>rand(1,4),
                    
                ];
                
                $this->db->table('student_apps')->insert($data);
       }

    }
}
