<?php

namespace App\Controllers\Permohonan;

use PDO;
use App\Controllers\User;
use App\Models\StudentModel;
use App\Helpers\student_helper;
use App\Models\StudentAppsModel;
use App\Controllers\BaseController;

class Pelajar extends BaseController
{

    protected $valicustom = [
        'name' => 'required|max_length[255]',
        'address' => 'required',
        'email'    => 'required|valid_email',
        'ic' => 'required',
        'id_course1' => [
            'label'  => 'First Course ',
            'rules'  => 'required',
            'errors' => [
                'required' => 'The {field} field is required.',
            ],
        ],

        'id_course3' => 'required',

    ];
    
    public function index()
    {
        // $name= $this->request->getVar('name');

        // dd($name);

        $d['title']='Pelajar';

       $model= new StudentModel();

    //    $model->select('student.*,c1.course_name as course1,
    //    c2.course_name as course2');
    //    $model->join('course as c1', 'c1.id = student_apps.id_course1');
    //    $model->join('student_apps', 'student_apps.id_student = student.id');
    //    $model->join('course as c2', 'c2.id = student_apps.id_course2');
    //    $model->join('course as c3', 'c3.id = student_apps.id_course3');



    //    if($this->request->getVar('name')??''){
    //    $model->like('name', $this->request->getVar('name'), 'both');
    //    }

    //    if($this->request->getVar('address')??''){
    //     $model->like('address', $this->request->getVar('address'), 'both');
    //     }

        $model->search($this->request);

    //    $data=$model->paginate(10);

       $d['model']=$model->paginate(10,'std');
       $d['pager']=$model->pager;

      $dpager= student_helper::countFrom($model->pager);

        $d['count']=$dpager['count'];
        $d['show']=$dpager['show'];

        
        return view('pelajar/index',$d);
         
    }


    public function show($id)
    {

        $d['title']='Show Student Application';

        $d['course']= student_helper::getCourseAll();
        $model=new StudentModel();
        $data= $model->find($id);

        $modelapp=new StudentAppsModel();
       $dataapp= $modelapp->where(['id_student'=>$id])->first();

            // dd($dataapp);
        $d['model']=$data;
        $d['modelapp']=$dataapp;
        $d['validation']=\Config\Services::validation();

        return view('pelajar/show',$d);
    }

    public function new()
    {
        $d['title']='Add Student Application';

       $d['course']= student_helper::getCourseAll();
       $d['validation']=\Config\Services::validation();

       
            
            return view('pelajar/new',$d);
    }


    
    public function create()
    {
   
   $validation= $this->validate($this->valicustom);


   if(!$validation){
         return redirect()->back()->withInput();
   }


   $model = new StudentModel();

   $datapost=$this->request->getVar();



if($model->insert($datapost)){

    $id_student=$model->insertID;

    $datapost['id_student']=$id_student;
    // $datapost['id_course1']=$this->request->getVar('course1');

    $modelapplication=new StudentAppsModel();

    // $dataapp=[
    //     'id_course1'=>$this->request->getVar('course1'),
    //     'id_course2'=>$this->request->getVar('course2'),
    //     'id_course3'=>$this->request->getVar('course3'),
    //     'id_student'=>$id_student
    // ];

    if($modelapplication->insert($datapost)){
        return redirect()->to('permohonan/pelajar/show/'.$id_student)->with('status', 'berjaya');

        // return redirect()->route('admin-permohonan/pelajar',[$id_student])->with('foo', 'message');

    }



}


return redirect()->back()->withInput();

        dd($model->insertID);

            dd($_POST);



    }

    function edit($id){

        $d['title']='Edit Student Application';

        $d['course']= student_helper::getCourseAll();
        $model=new StudentModel();
        $data= $model->find($id);

        $modelapp=new StudentAppsModel();
       $dataapp= $modelapp->where(['id_student'=>$id])->first();

            // dd($dataapp);
            $d['id']=$id;
        $d['model']=$data;
        $d['modelapp']=$dataapp;
        $d['validation']=\Config\Services::validation();

        return view('pelajar/edit',$d);

    }


public function update($id){

    $validation= $this->validate($this->valicustom);


    if(!$validation){
          return redirect()->back()->withInput();
    }

   $model = new StudentModel();
   $datapost=$this->request->getVar();
   $datapost['id']=$id;

if($model->save($datapost)){

    $datapostapps=$this->request->getVar();

    
  
    $modelapplication=new StudentAppsModel();
    $modelapplication->where(['id_student'=>$id]);
    $modelapplication->set($datapostapps);

    if($modelapplication->update()){
        return redirect()->to('admin-permohonan/pelajar/'.$id.'/edit')->with('status', 'berjaya');
    }



    }

}


public function delete($id){

    if($this->request->getMethod()=='delete'){


    $modelapp=new StudentAppsModel();

    $modelapp->where(['id_student'=>$id]);

    if($modelapp->delete()){

        $model=new StudentModel();

        if($model->delete($id)){
            return redirect()->to('admin-permohonan/pelajar/')->with('status', 'berjaya');

        }

    }

   



}else{

    print('bukan method delete');
}




}


}
