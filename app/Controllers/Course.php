<?php

namespace App\Controllers;

use App\Models\CourseModel;
use App\Helpers\student_helper;

class Course extends BaseController
{
    public function index()
    {
        // $name= $this->request->getVar('name');

        // dd($name);

        $d['title']='course';

       $model= new CourseModel();


       $d['model']=$model->paginate(10,'std');
       $d['pager']=$model->pager;

      $dpager= student_helper::countFrom($model->pager);

        $d['count']=$dpager['count'];
        $d['show']=$dpager['show'];


         return view('course/index',$d);
    }


}
