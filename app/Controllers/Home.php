<?php

namespace App\Controllers;

use Dompdf\Dompdf;
use App\Models\StudentModel;
use Joli\JoliNotif\Notification;
use Joli\JoliNotif\NotifierFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Home extends BaseController
{
    public function index()
    {

        echo 'hello world';
        // return view('welcome_message');
    }


    public function testnotis($cust){
        // Create a Notifier
$notifier = NotifierFactory::create();

$cust=str_replace('_',' ',$cust);

// Create your notification
$notification =
    (new Notification())
    ->setTitle('Notification title')
    ->setBody('This is the body of your notification '.$cust)
    ->setIcon(__DIR__.'/path/to/your/icon.png')
    ->addOption('subtitle', 'This is a subtitle') // Only works on macOS (AppleScriptNotifier)
    ->addOption('sound', 'Frog') // Only works on macOS (AppleScriptNotifier)
;

// Send it
$notifier->send($notification);

    }

    public function notis(){

         shell_exec('php index.php home testnotis selamat_hari_raya');

      //  $last_line = system('php index.php home testnotis', $retval);

      //  dd($last_line);
    }

    public function dompdf(){

        $model=new StudentModel();
        $data= $model->find(6);
        $d['model']=$data;
        // instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml(view('pelajar/test',$d));

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
    }

    public function excell(){
        $spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'Hello World !');

$writer = new Xlsx($spreadsheet);
$writer->save('world.xlsx');

return $this->response->download('world.xlsx', null)->setFileName('world.xlsx');
    }

}
