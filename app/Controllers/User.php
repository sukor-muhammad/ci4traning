<?php

namespace App\Controllers;

use App\Models\Users;
use App\Controllers\BaseController;

class User extends BaseController
{


    public function index()
    {
        //

        $d['title']='Register User';

        // $d['course']= student_helper::getCourseAll();
        $d['validation']=\Config\Services::validation();
 
        $d['model']=new Users();

             
             return view('register/new',$d);

    }


    public function create(){

   
        $validation= $this->validate( [
            'fullname' => 'required|max_length[255]',
            'username' => 'required|max_length[30]|is_unique[users.username]',
            'email'    => 'required|valid_email|is_unique[users.email]',
            'password' => 'required|max_length[255]',
            'confirmpassword'=>'matches[password]'
        ]);


        if(!$validation){
              return redirect()->back()->withInput();
        }

        $datapost=$this->request->getVar();
        $model= new Users();

        $rawpassword=$this->request->getVar('password');

        $datapost['password']=password_hash($rawpassword,PASSWORD_DEFAULT);

        if($model->save($datapost)){

            $id=$model->insertID;
        
                return redirect()->to('register/show/'.$id)->with('status', 'berjaya');
        

        
        }

        return redirect()->back()->withInput();
        


    }

    public function show($id){

        $d['title']='show User';

        // $d['course']= student_helper::getCourseAll();
        
 
      $model= new Users();
       $d['model']=$model->find($id);
             
             return view('register/show',$d);

    }


    public function login(){

           $d['title']='login';
           $d['validation']=\Config\Services::validation();

             return view('register/login',$d);

    }

    public function loginin(){

        $validation= $this->validate( [
            'username' => 'required',
            'password' => 'required',
           
        ]);


        if(!$validation){
              return redirect()->back()->withInput();
        }

        $password=$this->request->getVar('password');

        $username=$this->request->getVar('username');      
         $model=new Users();

       $data= $model->where(['username'=>$username])->first();

    //    dd($data);

       if($data){

        $authpassword=password_verify($password,$data->password);

        if($authpassword){ 

            $sesdata=[
                'id'=>$data->id,
                'fullname'=>$data->fullname,
                'email'=>$data->email,
                'isLoggedIn'=>TRUE
            ];

            $this->session->set($sesdata);

            return redirect()->to('admin-permohonan/pelajar');

        }else{
            return redirect()->back()->withInput()->with('status','password salah');
        }


       }else{
        return redirect()->back()->withInput()->with('status','tiada user');
       }




        
    }


    public function logout(){
    
        $this->session->destroy();

        return redirect()->to('login');

    }


}
