<?php

namespace App\Filters;

use App\Models\UserRole;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class AuthGuard implements FilterInterface
{
    /**
     * Do whatever processing this filter needs to do.
     * By default it should not return anything during
     * normal execution. However, when an abnormal state
     * is found, it should return an instance of
     * CodeIgniter\HTTP\Response. If it does, script
     * execution will end and that Response will be
     * sent back to the client, allowing for error pages,
     * redirects, etc.
     *
     * @param RequestInterface $request
     * @param array|null       $arguments
     *
     * @return mixed
     */
    public function before(RequestInterface $request, $arguments = null)
    {

  
        $login=session()->get('isLoggedIn');
        if(!$login){

            return redirect()->to('login');

        }

        if($arguments){
            $roleid=$arguments[0];
            $userid=session()->get('id');
            $model=new UserRole();

            $model->join('role','role.id=user_role.role_id');
            $model->whereIn('role_id',$arguments);
           $data= $model->where(['user_id'=>$userid ])->first();
            
           if(!$data){
                return redirect()->to('login');
            }

        }


    }

    /**
     * Allows After filters to inspect and modify the response
     * object as needed. This method does not allow any way
     * to stop execution of other after filters, short of
     * throwing an Exception or Error.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array|null        $arguments
     *
     * @return mixed
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        //
    }
}
