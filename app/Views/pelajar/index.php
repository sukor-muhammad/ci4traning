
<?php
$session = session();
?>
<?= $this->extend('layouts/main') ?>

<?= $this->section('content') ?>

<form action="" method="GET">

  <?=$this->include('pelajar/_formsearch')?>

</form>
<div class="form-group row text-left">
  <div class="col-sm-10">
    <a href="<?=site_url('admin-permohonan/pelajar/new')?>" class="btn btn-info">Add</a>
  </div>
</div>


<?php if($session->has('status')){?>

  <div class="alert alert-primary" role="alert">
  <?=$session->get('status')?>
</div>


<?php }?>


<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name </th>
      <th scope="col">Address</th>
      <th scope="col">Email</th>
      <th scope="col">course 1</th>
      <th scope="col">course 2</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($model as $row){ ?>
    <tr>
      <th scope="row"><?=++$count?></th>
      <td><?= $row['name']?></td>
      <td><?= $row['address']?></td>
      <td><?= $row['email']?></td>
      <td><?= $row['course1']?></td>
      <td><?= $row['course2']?></td>
      <td>   
        <a href="<?=site_url('admin-permohonan/pelajar/'.$row['id'].'/edit/')?>"
         class="btn btn-info"><i class="fas fa-edit"></i></a>
         <form class='d-inline-flex' id='deletestudent<?=$row['id']?>' action="<?=site_url("admin-permohonan/pelajar/".$row['id'])?>"
          method="post">
           <input type="hidden" name="_method" value="DELETE" />
           <?= csrf_field() ?>
           <button data-id='deletestudent<?=$row['id']?>' type="button" 
           class="btn btn-danger btndelete">
           <i class="fas fa-trash-alt"></i>
          </button>
         

         </form>

         <!-- <a href="<?//=site_url('permohonan/pelajar/delete/'.$row['id'])?>"
         class="btn btn-info"><i class="fas fa-edit"></i></a> -->

 </td>
    </tr>
    <?php } ?>
  </tbody>
</table>

<?=$show?>
<?= $pager->links('std','bootstrap_pagination') ?>

<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
  <script>

$( document ).ready(function() {


  <?php if($session->has('status')){?>

Swal.fire({
  icon: 'info',
  title: '<?=$session->get('status')?>',
  showConfirmButton: true,
})

<?php }?>




  $( ".btndelete" ).click(function(e) {

    e.preventDefault();

   var idform= $(this).data('id')

    var form=$('#'+idform);

    Swal.fire({
  title: 'Do you want to save the changes?',
  showDenyButton: true,
  showCancelButton: true,
  confirmButtonText: 'Save',
  denyButtonText: `Don't save`,
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {

    Swal.fire({
  icon: 'info',
  title: 'Please wait',
  showConfirmButton: false,
  allowOutsideClick: false,
  allowEscapeKey: false,
closeOnClickOutside:false
})

 form.submit();
  // Swal.fire('Saved!', '', 'success')
  } else if (result.isDenied) {
    Swal.fire('Changes are not saved', '', 'info')
  }
})

});



});

  </script>
  
  <?= $this->endSection() ?>