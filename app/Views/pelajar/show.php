<?php
$request = \Config\Services::request();
$session = session();
?>
  <?= $this->extend('layouts/main') ?>

<?= $this->section('content') ?>

<?= $validation->listErrors() ?>

<?php if($session->has('status')){?>

  <div class="alert alert-primary" role="alert">
  <?=$session->get('status')?>
</div>


<?php }?>


<form id='formstudent' action="<?=site_url('admin-permohonan/pelajar')?>" method="post">
<?= csrf_field() ?>

<?=$this->include('pelajar/_form')?>

</form>
  <?= $this->endSection() ?>

  <?= $this->section('scripts') ?>
  <script>

$( document ).ready(function() {


  <?php if($session->has('status')){?>

Swal.fire({
  icon: 'info',
  title: '<?=$session->get('status')?>',
  showConfirmButton: true,
})

<?php }?>




  $( ".addbutton" ).click(function(e) {

    e.preventDefault();
    var form=$('#formstudent');

    Swal.fire({
  title: 'Do you want to save the changes?',
  showDenyButton: true,
  showCancelButton: true,
  confirmButtonText: 'Save',
  denyButtonText: `Don't save`,
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {

    Swal.fire({
  icon: 'info',
  title: 'Please wait',
  showConfirmButton: false,
  allowOutsideClick: false,
  allowEscapeKey: false,
closeOnClickOutside:false
})

// form.submit();
  // Swal.fire('Saved!', '', 'success')
  } else if (result.isDenied) {
    Swal.fire('Changes are not saved', '', 'info')
  }
})

});



});

  </script>
  
  <?= $this->endSection() ?>