<?php
$request = \Config\Services::request();

?>
  <?= $this->extend('layouts/main') ?>

<?= $this->section('content') ?>

<?= $validation->listErrors() ?>

<form id='formstudent' action="<?=site_url('admin-permohonan/pelajar')?>" method="post">
<?= csrf_field() ?>


<div class="form-group row">
    <label for="course1" class="col-sm-2 col-form-label"> First Course </label>
    <div class="col-sm-10">

    <select name ='id_course1' class="form-control" id="course1">
    <option value=''>choose one</option>
  <?php 
if($course){
?>
 <?php 
foreach($course as $keyc => $rowc){
?>
      <option   <?=(old('course1')==$keyc)?'selected':''  ?>    value='<?=$keyc?>'><?=$rowc?></option>
<?php
}
 ?>
<?php
}
 ?>

    </select>

    </div>
  </div>

  <div class="form-group row">
    <label for="id_course2" class="col-sm-2 col-form-label"> Second  Course </label>
    <div class="col-sm-10">

    <select name ='id_course2' class="form-control" id="course1">
    <option value=''>choose one</option>
  <?php 
if($course){
?>
 <?php 
foreach($course as $keyc => $rowc){
?>
      <option <?=(old('course2')==$keyc)?'selected':''  ?>  value='<?=$keyc?>'><?=$rowc?></option>
<?php
}
 ?>
<?php
}
 ?>

    </select>

    </div>
  </div>
  <div class="form-group row">
    <label for="course3" class="col-sm-2 col-form-label"> Third  Course </label>
    <div class="col-sm-10">

    <select name ='id_course3' class="form-control" id="course1">
    <option value=''>choose one</option>
  <?php 
if($course){
?>
 <?php 
foreach($course as $keyc => $rowc){
?>
      <option value='<?=$keyc?>'><?=$rowc?></option>
<?php
}
 ?>
<?php
}
 ?>

    </select>

    </div>
  </div>


  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-10">
      <input value='<?=old('name')?>' name='name' type="text"
       class="form-control
       <?= ($validation->getError('name'))?'is-invalid':''  ?> 
       " 
       id="inputEmail3" placeholder="name">
       <div class="invalid-feedback">
          <?=$validation->getError('name')?>
        </div>
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Address	</label>
    <div class="col-sm-10">
      <input value='<?=$request->getVar('address')?>'  name='address' type="text" class="form-control" id="inputPassword3" placeholder="Address">
    </div>
  </div>

  
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">email</label>
    <div class="col-sm-10">
      <input value='<?=$request->getVar('name')?>' name='email' type="email" class="form-control" id="inputEmail3" placeholder="name">
    </div>
  </div>

  
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">ic</label>
    <div class="col-sm-10">
      <input value='<?=$request->getVar('name')?>' name='ic' type="text" class="form-control" id="inputEmail3" placeholder="name">
    </div>
  </div>
  
 
  </div>
  <div class="form-group row text-center">
    <div class="col-sm-10">
      <button type="button" class="btn btn-primary addbutton">Add</button>
      <button type="reset" class="btn btn-info">Reset</button>
    </div>
  </div>

</form>
  <?= $this->endSection() ?>

  <?= $this->section('scripts') ?>
  <script>

$( document ).ready(function() {

  $( ".addbutton" ).click(function(e) {

    e.preventDefault();
    var form=$('#formstudent');

    Swal.fire({
  title: 'Do you want to save the changes?',
  showDenyButton: true,
  showCancelButton: true,
  confirmButtonText: 'Save',
  denyButtonText: `Don't save`,
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {

    Swal.fire({
  icon: 'info',
  title: 'Please wait',
  showConfirmButton: false,
})

form.submit();
  // Swal.fire('Saved!', '', 'success')
  } else if (result.isDenied) {
    Swal.fire('Changes are not saved', '', 'info')
  }
})

});



});

  </script>
  
  <?= $this->endSection() ?>