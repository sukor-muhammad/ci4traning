<?php
$request = \Config\Services::request();

?>
  <?= $this->extend('layouts/main') ?>

<?= $this->section('content') ?>

<?= $validation->listErrors() ?>

<form id='formstudent' action="<?=site_url('register/create')?>" method="post">
<?= csrf_field() ?>

<?= $this->include('register/_form') ?>
 
<div class="form-group row text-center">
    <div class="col-sm-10">
      <button type="button" class="btn btn-primary addbutton">submit</button>
      <button type="reset" class="btn btn-info">Reset</button>
    </div>
  </div>

</form>
  <?= $this->endSection() ?>

  <?= $this->section('scripts') ?>
  <script>

$( document ).ready(function() {

  $( ".addbutton" ).click(function(e) {

    e.preventDefault();
    var form=$('#formstudent');

    Swal.fire({
  title: 'Do you want to save the changes?',
  showDenyButton: true,
  showCancelButton: true,
  confirmButtonText: 'Save',
  denyButtonText: `Don't save`,
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {

    Swal.fire({
  icon: 'info',
  title: 'Please wait',
  showConfirmButton: false,
})

form.submit();
  // Swal.fire('Saved!', '', 'success')
  } else if (result.isDenied) {
    Swal.fire('Changes are not saved', '', 'info')
  }
})

});



});

  </script>
  
  <?= $this->endSection() ?>