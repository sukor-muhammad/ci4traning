
<?php
$session = session();
?>

<?= $this->extend('layouts/mainlogin') ?>

<?= $this->section('content') ?>

<form id='formstudent' action="<?=site_url('loginin')?>" method="post">
<?= csrf_field() ?>
<div class="form-group row">
    <label for="username" class="col-sm-2 col-form-label">username</label>
    <div class="col-sm-10">
      <input value='<?=old('username')?>' name='username' type="text"
       class="form-control
       <?= ($validation->getError('username'))?'is-invalid':''  ?> 
       " 
       id="username" placeholder="username">
       <div class="invalid-feedback">
          <?=$validation->getError('username')?>
        </div>
    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">password</label>
    <div class="col-sm-10">
      <input value='<?=old('password')?>' name='password' type="password"
       class="form-control
       <?= ($validation->getError('password'))?'is-invalid':''  ?> 
       " 
       id="password" placeholder="password">
       <div class="invalid-feedback">
          <?=$validation->getError('password')?>
        </div>
    </div>
  </div>

<div class="form-group row text-center">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary addbutton">submit</button>
    </div>
  </div>

</for>

<?= $this->endSection() ?>
