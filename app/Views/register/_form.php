
 


  <div class="form-group row">
    <label for="fullname" class="col-sm-2 col-form-label">fullname</label>
    <div class="col-sm-10">
      <input value='<?=old('fullname',$model->fullname)?>' name='fullname' type="text"
       class="form-control
       <?= ($validation->getError('fullname'))?'is-invalid':''  ?> 
       " 
       id="fullname" placeholder="fullname">
       <div class="invalid-feedback">
          <?=$validation->getError('fullname')?>
        </div>
    </div>
  </div>

  <div class="form-group row">
    <label for="username" class="col-sm-2 col-form-label">username</label>
    <div class="col-sm-10">
      <input value='<?=old('username',$model->username)?>' name='username' type="text"
       class="form-control
       <?= ($validation->getError('username'))?'is-invalid':''  ?> 
       " 
       id="username" placeholder="username">
       <div class="invalid-feedback">
          <?=$validation->getError('username')?>
        </div>
    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">password</label>
    <div class="col-sm-10">
      <input value='<?=old('password',$model->password)?>' name='password' type="password"
       class="form-control
       <?= ($validation->getError('password'))?'is-invalid':''  ?> 
       " 
       id="password" placeholder="password">
       <div class="invalid-feedback">
          <?=$validation->getError('password')?>
        </div>
    </div>
  </div>

  <div class="form-group row">
    <label for="confirmpassword" class="col-sm-2 col-form-label">confirmpassword</label>
    <div class="col-sm-10">
      <input value='<?=old('confirmpassword')?>' name='confirmpassword' type="password"
       class="form-control
       <?= ($validation->getError('confirmpassword'))?'is-invalid':''  ?> 
       " 
       id="confirmpassword" placeholder="confirmpassword">
       <div class="invalid-feedback">
          <?=$validation->getError('confirmpassword')?>
        </div>
    </div>
  </div>

  <div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">email</label>
    <div class="col-sm-10">
      <input value='<?=old('email',$model->email)?>' name='email' type="text"
       class="form-control
       <?= ($validation->getError('email'))?'is-invalid':''  ?> 
       " 
       id="email" placeholder="password">
       <div class="invalid-feedback">
          <?=$validation->getError('email')?>
        </div>
    </div>
  </div>

  
  
 
  </div>
