<?php

namespace App\Models;

use CodeIgniter\Model;

class StudentAppsModel extends Model
{
    protected $table      = 'student_apps';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_student', 'id_course1','id_course2','id_course3'];
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'update_at';
    protected $deletedField  = 'delete_at';

}