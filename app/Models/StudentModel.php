<?php

namespace App\Models;

use CodeIgniter\Model;

class StudentModel extends Model
{
    protected $table      = 'student';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'address','email','ic'];
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'update_at';
    protected $deletedField  = 'delete_at';

    protected $useTimestamps = true;

    public function search($request){

        
       $this->select('student.*,c1.course_name as course1,
       c2.course_name as course2');
       $this->join('student_apps', 'student_apps.id_student = student.id');
       $this->join('course as c1', 'c1.id = student_apps.id_course1');
       $this->join('course as c2', 'c2.id = student_apps.id_course2');
       $this->join('course as c3', 'c3.id = student_apps.id_course3');
    //    $this->withDeleted();

        if($request->getVar('name')??''){

            $this->like('name', $request->getVar('name'), 'both');

        }

        if($request->getVar('address')??''){

            $this->like('address', $request->getVar('address'), 'both');

        }


        $this->orderBy('created_at', 'DESC');

        // if($request->getVar('course')??''){
        //     $course=$request->getVar('course');
        //     $course='Accountancy';

        //     $where = "(c1.course_name like '%$course%' 
        //     OR c2.course_name like '%$course%'   
        //     OR c3.course_name like '%$course%'  )";
        //     $this->where($where);

        //      $this->like('c2.course_name', 'Engineering', 'both');

        // }
        

    }



}